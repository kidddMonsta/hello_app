class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def you
    render html: "You can do it!"
  end
end
